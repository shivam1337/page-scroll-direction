let initialTop = 0

window.addEventListener('scroll', (e) => {
    const currentScrollPosition = window.scrollY
    if (initialTop < currentScrollPosition) {
        // On scroll direction down
        console.log('Scroll direction: Down')
        // Code when scroll direction down
    } else {
        // On scroll direction up
        console.log('Scroll direction: Up')
        // Code when scroll direction up
    }
    initialTop = currentScrollPosition
})
